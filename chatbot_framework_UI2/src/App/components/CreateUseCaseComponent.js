import React, { Component } from 'react';
import { Button,Form, Row,Col } from 'react-bootstrap';
//import "../../../node_modules/font-awesome/scss/font-awesome.scss";
//  import 'bootstrap/dist/css/bootstrap.min.css';
import { FaPen, FaTrash } from 'react-icons/fa';
//https://www.figma.com/proto/bpWqFHG1AtO7n5Q0NxhZ4j/IDIL-wireframe?node-id=1%3A2&scaling=min-zoom
class CreateUseCase extends Component {

    constructor() {
        super() 
        this.state = {
            usecases :[ {
                usecaseTitle :'chatbotusecase1'
            },{
                usecaseTitle :'chatbotusecase2'
            },{
                usecaseTitle :'chatbotusecase3'
            }]
        }
    }

    editUsacase = () =>{
        console.log('onEdit.......')
    }

    deleteUsecase = () => {
        console.log('onDelete.......')
    }

    addUsecase = () => {
        console.log('added use cases')
        const newUsecase = [
            ...this.state.usecases,
            {usecaseTitle : 'usecaseTitleAdded'},
          ];
        this.setState({usecases : newUsecase})
    }

    render() {
        return (
            <div className="container"> <h3> Use cases of project </h3>

            <div style = {{width : '1000px',overflowY :'scroll',height:'250px'}}>           
                 {this.state.usecases.map(useCase =>{
                return <div className = "form-control" style = {{backgroundColor :'white' ,display : 'flex' ,flexDirection :'row', marginTop :'10px',border:'1px solid black',borderRadius : '5px'}}>
                         <div style = {{flex :'1 0 0px',flexBasis:'25em',float:'left',overflow :'hidden'}}><h5>{useCase.usecaseTitle}</h5></div>
                         <div style = {{flex :'0 50px 0',flexBasis:'25em',float:'right',marginLeft :"50%"}}><button  style={{
                              borderRadius: "0px",
                              outline: "0",
                              borderWidth: "0px",
                              backgroundColor: "white",
                            }} onClick = {this.editUsacase}><FaPen/></button></div>
                         <div style = {{flex :'0 0 50px'}}> <button  style={{
                              borderRadius: "0px",
                              outline: "0",
                              borderWidth: "0px",
                              backgroundColor: "white",
                            }} onClick = {this.deleteUsecase}><FaTrash/></button></div>
                     <br></br> </div>
            })
            }
            </div>
             
             <Button  onClick={this.addUsecase}  className="float-right btn btn-light" style={{
                              borderRadius: "5px",
                              outline: "0",
                              borderWidth: "0px",
                              backgroundColor: "green",
                              color:'white',
                              marginTop :'20px'
                            }}>
             ADD
             </Button>

            </div>
        )
    }


}
export default CreateUseCase;