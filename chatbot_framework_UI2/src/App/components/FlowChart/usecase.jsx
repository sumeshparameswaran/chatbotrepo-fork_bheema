import React, { useState } from "react";
import Message from "./message";
import MessageBot from "./botmessaage";
import { Handle } from "react-flow-renderer";
import { Input, Button } from "reactstrap";

const customNodeStyles = {
  background: "#f7f5f0",
  color: "#000",
  padding: 10,
  width: 270,
};

const PathComponent = ({ data }) => {
  const [isuserHidden, setuserisHidden] = useState(true);
  const [isbotHidden, setbotisHidden] = useState(true);
  const [components, setComponents] = useState([]);
  const [messagechanged,setMessagechanged] = useState('');

  const toggleUserHidden = () => {
    setuserisHidden(false);
    setbotisHidden(true);
  };

  const toggleBotHidden = () => {
    setuserisHidden(true);
    setbotisHidden(false);
  };

 const showMessage= (message,e) => {
    e.preventDefault();
    console.log("messages:",message)
    setMessagechanged(message[0]);
    //document.getElementById('message').value= message[0];
  }

  const handleMessage = (message, isUser) => {
    console.log(isUser);
    const newComponents = [
      ...components,
      <div>
      <div   
        key={components.length}
        style={{
          backgroundColor: "white",
          padding: 5,
          borderRadius: 10,
          borderTopLeftRadius: isUser ? 10 : 0,
          borderTopRightRadius: isUser ? 0 : 10,
          margin: 5,
          display: "flex",
          justifyContent: isUser ? "flex-end" : "flex-start",
        }}
  
      >  
        {isUser ? <div key = {message.length}>
          <select key = {message.length-1}>
          {
            message.map(x=>{
              return <option  name = "title" key = {x} value= {x}>{x}</option>
            })
          }
          </select>
        </div> :<div>{message}</div>}
      </div>
       </div>,
    ];
    setComponents(newComponents);
  };

  const handleCloseBot = () => {
    setbotisHidden(true);
  }

  const handleCloseUser = () => {
    setuserisHidden(true);
  }

  return (
    <div style={customNodeStyles}>
      <Handle type="target" position="top" />
      <div>
        <Input
          type="text"
          style={{ height: 20, fontSize: 15 }}
          placeholder="title"
        />
      </div>
      <br />
      {components.length === 0
        ? null
        : components.map((component) => component)}
      <div className="container">
        <Button
          outline
          color="primary"
          size="sm"
          onClick={toggleUserHidden.bind(this)}
        >
          <div>User Says</div>
        </Button>
        <Button
          outline
          color="success"
          size="sm"
          style={{ float: "right" }}
          onClick={toggleBotHidden.bind(this)}
        >
          Bot Says
        </Button>
      </div>
      {/* <Handle type="source" position="bottom" /> */}
      <br />
      {!isuserHidden && <Message handleMessage={handleMessage} handleClose={handleCloseUser}/>}
      {!isbotHidden && <MessageBot handleMessage={handleMessage} handleClose={handleCloseBot} />}
    </div>
  );
};
export default PathComponent;
