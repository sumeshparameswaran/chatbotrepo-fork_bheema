import React, { useState, Fragment } from "react";
import PathComponent from "./usecase";

import ReactFlow, { addEdge, Background, Controls, removeElements } from "react-flow-renderer";

const initialElements = [
  {
    id: "1",
    type: "special",
    data: {
      text: 'A custom node'
    },
    position: { x: 0, y: 0 },
  },
];
const onLoad = (reactFlowInstance) => {
  //reactFlowInstance.fitView();
};
const nodeTypes = {
  special: PathComponent,
};

const MindNode = () => {
  const [elements, setElements] = useState(initialElements);

  const addNode = () => {
    setElements((e) =>
      e.concat({
        id: (e.length + 1).toString(),
        data: { text: 'A custom node' },
        type: "special",
        position: {
          x: 10,
          y: 10,
        },
      })
    );
  };

  const onConnect = (params) => setElements((e) => addEdge(params, e));
  const onElementsRemove = (elementsToRemove) =>
    setElements((els) => removeElements(elementsToRemove, els));

  return (
    <Fragment>
      <ReactFlow
        elements={elements}
        nodeTypes={nodeTypes}
        onLoad={onLoad}
        style={{ width: "100%", height: "90vh",backgroundColor:'white' }}
        onConnect={onConnect}
        onElementsRemove={onElementsRemove}
        connectionLineStyle={{ stroke: "black", strokeWidth: 1 }}
        connectionLineType="bezier"
        snapToGrid={true}
        snapGrid={[16, 16]}
        defaultZoom={1.2}
        
      >
        <Background color="blue" gap={0}  />

        <Controls/>
      </ReactFlow>

      <div>
        {/* <input
          type="text"
          onChange={(e) => setName(e.target.value)}
          name="title"
        /> */}
        <button type="button" className = 'fa fa-plus' style={{float:'right' ,
        borderRadius:'50px',
        //marginTop:'10px',
        color:'#FFF',
        backgroundColor:'#0C9',
        height:'60px',
        width:'60px',
        bottom:'40px',
        right:'40px',
        marginBottom:'200px'
         }} onClick={addNode}>
         
        </button>
      </div>
    </Fragment>
  );
};

export default MindNode;
