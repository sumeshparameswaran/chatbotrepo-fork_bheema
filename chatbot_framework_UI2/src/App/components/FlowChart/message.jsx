import React, { Component } from "react";
import { Button } from "reactstrap";
import TextareaAutosize from "react-textarea-autosize";
import { FaTimes } from "react-icons/fa";


class Message extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tempmessage : '',
      showModalpop : false,
       Utterances : []
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onClose = this.onClose.bind(this);
    this.handleUtterances = this.handleUtterances.bind(this);
  }


  Showpop =()=>{
    const disabled = this.state.tempmessage.length<1;
    return (  
      <div style = {{position:'absolute',bottom:'60px',width:'40%',height:'40%',backgroundColor:'white', justifyContent:"space-around"} }>  
      <div>  
       
      <button disabled = {disabled} style = {{ borderRadius : '0px', outline: "0", borderWidth: "0px",backgroundColor:'white'}}onClick={this.handleUtterances}>AddnewItem</button>
       <hr/>
      <button style = {{ borderRadius : '0px', outline: "0", borderWidth: "0px",backgroundColor:'white'}}onClick={this.hideModalpopbutton}>closePopup</button>  
      </div>  
      </div>  
      );
  }
  handleChange=(event) =>{
    const input = event.target.value;
    this.setState({ tempmessage: input }); 
    
  }
  handleSubmit(event) {
    if(this.state.tempmessage !== '') {
        const newElement = this.state.Utterances;
        newElement.push(this.state.tempmessage);
        this.setState({Utterances :newElement,tempmessage:''})
    }

    event.preventDefault();
    this.props.handleMessage(this.state.Utterances, true);
    this.setState({Utterances :[],tempmessage:''})
  }

  showModalpopbutton = () =>{
    this.setState({showModalpop:true})
  }

  hideModalpopbutton = () =>{
    this.setState({showModalpop:false})
  }

  handleUtterances(event) {
    this.setState({showModalpop:false})
    event.preventDefault();
    if(this.state.tempmessage !== '') {
     const newElement = this.state.Utterances;
     newElement.push(this.state.tempmessage);
     this.setState({Utterances :newElement,tempmessage:''})
    }

  }
  onClose() {
    this.props.handleClose();
  }
  render() {
    return (
      <div style={{ borderRadius: 5, backgroundColor: "#007bff" }}>
        <div
          style={{
            paddingLeft: 5,
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <div>User</div>
          <FaTimes
            color="black"
            style={{ paddingTop: 5 }}
            onClick={this.onClose}
          ></FaTimes>
        </div>
        <div style={{ backgroundColor: "white", padding: 10, height: "auto" }}>
          <TextareaAutosize
            style={{ fontSize: 15, width: 200 }}
            value={this.state.tempmessage}
            placeholder="Message"
            onChange={(e)=>this.handleChange(e)}
          />
          <div>   
         
          <Button  style = {{display:"inline-block",marginRight:'60px'}} size="sm" onClick={this.showModalpopbutton}>
            {this.state.Utterances.length}/{this.state.Utterances.length}
          </Button>
          {this.state.showModalpop ? <this.Showpop></this.Showpop> : null}
          <Button  style = {{display:"inline-block",marginRight:'5px'}} size="sm" onClick={this.handleSubmit}>
            Add
          </Button>
          </div>
        </div>
      </div>
    );
  }
}

export default Message;
