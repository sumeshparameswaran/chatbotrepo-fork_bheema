import React, { Component } from "react";

class SharedTextArea extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
    };
  }
  render() {
    return (
      <div className="form-group">
        <label htmlFor="descField" className="col-xs-5">
          Description
        </label>
        <div className="col-xs-10">
          <textarea
            type="text"
            className="form-control"
            id="descField"
          ></textarea>
        </div>
      </div>
    );
  }
}

export default SharedTextArea;
