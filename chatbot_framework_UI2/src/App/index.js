import React, { Component } from "react";
import SignUp1 from "./components/LoginComponent";
import { connect } from "react-redux";
import MenuComponent from "./layout/MenuComponent";
import { withRouter } from "react-router-dom";

import "../../node_modules/font-awesome/scss/font-awesome.scss";

class App extends Component {
  render() {
    return !this.props.isAuthenticated ? <SignUp1 error = {this.props.error}  loading = {this.props.loading}></SignUp1> : <MenuComponent />;
  }
}
const mapStateToProps = (state) => {
  return {
    isAuthenticated: localStorage.getItem("token") !== null,
    error: state.error,
    loading : state.loading
  };
};

export default withRouter(connect(mapStateToProps, null)(App));
