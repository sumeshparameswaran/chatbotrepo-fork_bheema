import * as actionTypes from "./actions";
import axios from "axios";

export const authStart = () => {
  return {
    type: actionTypes.AUTH_START,
    loading :true
  };
};

export const authSuccess = (token) => {
  return {
    type: actionTypes.AUTH_SUCCESS,
    token: token,
    loading :false
  };
};

export const authFail = (error) => {
  return {
    type: actionTypes.AUTH_FAIL,
    error: true,
    loading :false
  };
};

export const authLogin = (username, password) => {
  return dispatch => {
    dispatch(authStart());
    const credential = {
      username: username,
      password: password,
    };
    axios({
      method: "post",
      url: "http://127.0.0.1:8000/auth/",
      data: credential,
    })
      .then((response) => {
        localStorage.setItem("token", response.data.token);
        axios({
          method: "post",
          url: "http://127.0.0.1:8000/users/login",
          data: credential,
        }).then((response) => {
          localStorage.setItem("id", response.data.id);
          localStorage.setItem("role", response.data.role);
          dispatch(authSuccess(response.data.token));
        });
      })
      .catch((error) => {
        dispatch(authFail(error));
      });
  };
};

export const authLogout = () => {
  localStorage.removeItem("token");
  localStorage.removeItem("id");
  localStorage.removeItem("role");
  return {
    type: actionTypes.AUTH_LOGOUT,
  };
};
