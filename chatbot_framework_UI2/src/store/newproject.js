import * as actionTypes from "./actions";
import axios from "axios";

export const saveProjectStart = () => {
    console.log("save_project_start");
    return {
      type: actionTypes.SAVE_PROJECT_START,
      loading : true,
      error :  false
    };
  };
  
  export const saveProjectSuccess = (projectId) => {
    console.log("save_project_success");
    return {
      type: actionTypes.SAVE_PROJECT_SUCCESS,
      projectId: projectId,
      loading : false,
      error :  false
    };
  };

  
  export const saveProjectFail = (error) => {
    return {
      type: actionTypes.SAVE_PROJECT_FAIL,
      error: error,
      loading : false,
      error :  true
    };
  };

  export const saveProject = (projectName, description) => {
    console.log(projectName);
    return dispatch => {
      dispatch(saveProjectStart());
      debugger;
      const requestObj = {
        name: projectName,
        description: description,
        id:localStorage.getItem("id")
      };
      axios({
        method: "post",
        url: "http://127.0.0.1:8000/projects/newProject",
        data: requestObj,
      })
        .then((response) => {
          localStorage.setItem("projectId", response.data.projectId); 
            console.log(response)
            dispatch(saveProjectSuccess(response.data.projectId));
        })
        .catch((error) => {
          dispatch(saveProjectFail(console.error()));
        });
    };
  };  
  