import * as actionTypes from "./actions";
import config from "./../config";
import { updateObject } from "../utility";

const initialState = {
  isOpen: [], //for active default menu
  isTrigger: [], //for active default menu, set blank for horizontal
  ...config,
  isFullScreen: false, // static can't change
  token: null,
  id: null,
  role: null,
  error: null,
  loading: null,
  projectId: null,
  projects : null
};

const authStart = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: true,
  });
};

const authSuccess = (state, action) => {
  return updateObject(state, {
    token: action.token,
    error: null,
    loading: false,
  });
};

const authFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
  });
};

const authLogout = (state, action) => {
  return updateObject(state, {
    token: null,
    id: null,
    role: null,
  });
};

const saveProjectStart = (state, action) => {
  debugger;
  return updateObject(state, {
    error: null,
    loading: true,
  });
};

// const saveProjectStart = (state, action) => {
//   debugger;
//   return updateObject(state, {
//     error: null,
//     loading: true,
//   });
// };

const deleteProjectStart = (state, action) => {
  debugger;
  return updateObject(state, {
    error: null,
    loading: true,
  });
};

const projectLodabyuser = (state, action) => {
  debugger;
  return updateObject(state, {
    error: null,
    loading: true,
  });
};


const saveProjectSuccess = (state, action) => {
  return updateObject(state, {
    projectId: action.projectId,
    error: null,
    loading: false,
  });
};

export const projectloadbyusersuccess = (state, action) => {
  // console.log("delete_Project_Success");
   return updateObject(state, {
     projects: action.projects,
     error: null,
     loading: false,
   });
 };


export const deleteProjectSuccess = (state, action) => {
 // console.log("delete_Project_Success");
  return updateObject(state, {
    projects: action.projects,
    error: null,
    loading: false,
  });
};


const saveProjectFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
  });
};

const loadProjectFail = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
  });
};



const reducer = (state = initialState, action) => {
  let trigger = [];
  let open = [];

  switch (action.type) {
    case actionTypes.COLLAPSE_MENU:
      return {
        ...state,
        collapseMenu: !state.collapseMenu,
      };
    case actionTypes.COLLAPSE_TOGGLE:
      if (action.menu.type === "sub") {
        open = state.isOpen;
        trigger = state.isTrigger;

        const triggerIndex = trigger.indexOf(action.menu.id);
        if (triggerIndex > -1) {
          open = open.filter((item) => item !== action.menu.id);
          trigger = trigger.filter((item) => item !== action.menu.id);
        }

        if (triggerIndex === -1) {
          open = [...open, action.menu.id];
          trigger = [...trigger, action.menu.id];
        }
      } else {
        open = state.isOpen;
        const triggerIndex = state.isTrigger.indexOf(action.menu.id);
        trigger = triggerIndex === -1 ? [action.menu.id] : [];
        open = triggerIndex === -1 ? [action.menu.id] : [];
      }

      return {
        ...state,
        isOpen: open,
        isTrigger: trigger,
      };
    case actionTypes.NAV_CONTENT_LEAVE:
      return {
        ...state,
        isOpen: open,
        isTrigger: trigger,
      };
    case actionTypes.NAV_COLLAPSE_LEAVE:
      if (action.menu.type === "sub") {
        open = state.isOpen;
        trigger = state.isTrigger;

        const triggerIndex = trigger.indexOf(action.menu.id);
        if (triggerIndex > -1) {
          open = open.filter((item) => item !== action.menu.id);
          trigger = trigger.filter((item) => item !== action.menu.id);
        }
        return {
          ...state,
          isOpen: open,
          isTrigger: trigger,
        };
      }
      return { ...state };
    case actionTypes.FULL_SCREEN:
      return {
        ...state,
        isFullScreen: !state.isFullScreen,
      };
    case actionTypes.FULL_SCREEN_EXIT:
      return {
        ...state,
        isFullScreen: false,
      };
    case actionTypes.CHANGE_LAYOUT:
      return {
        ...state,
        layout: action.layout,
      };
    case actionTypes.AUTH_START:
      return authStart(state, action);
    case actionTypes.AUTH_SUCCESS:
      return authSuccess(state, action);
    case actionTypes.AUTH_FAIL:
      return authFail(state, action);
    case actionTypes.AUTH_LOGOUT:
      return authLogout(state, action);
    case actionTypes.SAVE_PROJECT_START:
      return saveProjectStart(state, action);
    case actionTypes.SAVE_PROJECT_SUCCESS:
      return saveProjectSuccess(state, action);
    case actionTypes.SAVE_PROJECT_FAIL:
      return saveProjectFail(state, action);
    case actionTypes.PROJECT_DELETE:
        return deleteProjectStart(state, action);
    case actionTypes.DELETE_PROJECT_SUCESS:
      return deleteProjectSuccess(state, action);   
      case actionTypes.PROJECT_LOAD_BY_USER:
        return projectLodabyuser(state, action);
    case actionTypes.PROJECT_LOAD_BY_USER_SUCCESS:
      return projectloadbyusersuccess(state, action);
    case actionTypes.PROJECT_LOAD_BY_USER_FAILED:
      return loadProjectFail(state, action);  
    default:
      return state;
  }
};

export default reducer;
