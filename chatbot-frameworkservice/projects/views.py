from rest_framework import viewsets
from .models import Project
from users.models import User
from .serializers import ProjectsSerializer
from rest_framework.response import Response
from rest_framework import viewsets, generics
from django.views.decorators.csrf import csrf_exempt
import json
from django.core import serializers
from django.http import HttpResponse

# Create your views here
class NewProjectViewSet(generics.GenericAPIView):
    serializer_class = ProjectsSerializer

    @csrf_exempt 
    def post(self, request, *args, **kwargs):
        projectInstance = Project(
            name = request.data["name"],
            description = request.data["description"],
            project_user = User.objects.get(id = request.data["id"])
        )
        projectInstance.save()
        return Response({"success":"success"})

class LoadProjectsByUserViewSet(generics.GenericAPIView):
    serializer_class = ProjectsSerializer

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        id = request.data["user_id"]
        results = Project.objects.filter(project_user_id=id)
        projects = []
        print(results.count())
        for i in range(0, results.count()):
            project = {
                "id" : results[i].id,
                "name" : results[i].name,
                "description" : results[i].description
            }
            projects.append(project)
        tmpJson = serializers.serialize("json",results)
        tmpObj = json.loads(tmpJson)

        return Response({"projects":projects})

class DeleteProjectViewSet(generics.GenericAPIView):
    serializer_class = ProjectsSerializer

    @csrf_exempt
    def post(self, request , *args, **kwargs):
        id = request.data["project_id"]
        project = Project.objects.get(id = id)
        project.delete()

        return Response({"success":"success"})