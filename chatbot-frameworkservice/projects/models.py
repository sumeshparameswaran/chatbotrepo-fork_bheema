from django.db import models
from users.models import User

# Create your models here.
class Project(models.Model):
    name = models.CharField(max_length=50, default="Default_Project")
    description = models.TextField(max_length=500,  default=None)
    project_user=models.ForeignKey(User, on_delete=models.CASCADE, default=1)